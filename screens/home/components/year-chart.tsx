import {Dimensions, Text, View} from "react-native";
import * as React from 'react';
import {LineChart} from "react-native-chart-kit";
import {useEffect} from "react";

// @ts-ignore
export default function YearChartComponent(props) {


    useEffect(() => {
console.log("received data is",props.chartType)
    },[])

return(

    <View>
        <LineChart
            data={props.dataProp}
            width={Dimensions.get("window").width} // from react-native
            height={220}
            yAxisLabel="$"
            yAxisSuffix="k"
            yAxisInterval={1} // optional, defaults to 1
            chartConfig={{
                backgroundColor: "#4169E1",
                backgroundGradientFrom: "#4169E1",
                backgroundGradientTo: "#f5f2d0",
                decimalPlaces: 1, // optional, defaults to 2dp
                color: (opacity = 1) => `rgba(255, 0, 0, ${opacity})`,
                labelColor: (opacity = 0) => `rgba(255, 255, 255, ${opacity})`,
                style: {
                    borderRadius: 16
                },
                propsForDots: {
                    r: "6",
                    strokeWidth: "2",
                    stroke: "#fff"
                }
            }}
            style={{
                marginVertical: 0,
                borderRadius: 5,


            }}
        />
    </View>
)





}