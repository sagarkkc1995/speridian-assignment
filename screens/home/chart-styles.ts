import {StyleSheet} from "react-native";

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor : 'black'
    },
    headerStyle : {
        height : '70%'

    },

    title: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    separator: {
        marginVertical: 30,
        height: 1,
        width: '80%',
    },
    buttonStyle: {
        backgroundColor: 'rgba(20,174,255,0.51)',
        justifyContent: 'center',
        alignContent: 'center',
        borderWidth: 1,
        borderRadius: 10,
        width: 60,
        height: 30,
    },
    loaderStyle : {
        width: 200,
        height: 200,
        backgroundColor: '#000',
        alignSelf: 'center'
    }
});
