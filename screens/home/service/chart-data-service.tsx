import { create } from 'apisauce'


const client = "5f91a35e237f740013e6e98c"
const secret = "aa2ecefc9db260bf926a7a40e0f1ef"

const tokenModel = {
    client_id :  client,
    secret : secret,
    institution_id : 'ins_3',
    initial_products : ["auth"],
    options  :{
        webhook: "https://www.genericwebhookurl.com/webhook"
    }
}



     export const  getToken = async (callback) => {

        try {

            const api = create({baseURL: 'https://sandbox.plaid.com'})


            api.post('/sandbox/public_token/create', tokenModel)  .then(response => {
                // @ts-ignore
                console.log('public token from file itself', response.data.public_token)
                // @ts-ignore
            callback(response.data.public_token)
            })
                .then(console.log)


            // @ts-ignore


        }
        catch (ex){
            alert(ex)
        }

    }


export const getAccess = async (token : string,callback : any) => {

    const ExchangeModal = {
        client_id :  client,
        secret : secret,
        public_token : token
    }
    try {

        const api = create({baseURL: 'https://sandbox.plaid.com'})


        api.post('/item/public_token/exchange', ExchangeModal)  .then(response => {
            // @ts-ignore
            console.log('inner access token is', response.data.access_token)
            // @ts-ignore

            return callback(response.data.access_token)
            // fireWebHook(response.data.access_token)
        })
            .then(console.log)


        // @ts-ignore


    }
    catch (ex){
        alert(ex)
    }

}


export const fireHook = async (token: string,callback : any) => {
    const webHookModal = {
        client_id: client,
        secret: secret,
        // access_token : token,
        access_token: "access-sandbox-e3ff8398-dbc0-462e-9ff7-d8ecadac468e",
        webhook_code: "DEFAULT_UPDATE"
    }
    try {

        const api = create({baseURL: 'https://sandbox.plaid.com'})


        api.post('/sandbox/item/fire_webhook', webHookModal).then(response => {
            // @ts-ignore
            console.log('webhook data is ', response)

            callback(response)
            // @ts-ignore
            // getTranscations("access-sandbox-e3ff8398-dbc0-462e-9ff7-d8ecadac468e")
        })
            .then(console.log)


        // @ts-ignore


    } catch (ex) {
        alert(ex)
    }

}

export const retreiveTransactions = async (callback : any) => {
    const transactionModal = {
        client_id: client,
        secret: secret,
        access_token: "access-sandbox-e3ff8398-dbc0-462e-9ff7-d8ecadac468e",
        start_date: "2020-01-01",
        end_date: "2020-09-02"
    }
    try {

        const api = create({baseURL: 'https://sandbox.plaid.com'})


        api.post('/transactions/get', transactionModal).then(response => {
            // @ts-ignore
            console.log('transactions are', response)
            // @ts-ignore
            // filterData(response.data.transactions)
            callback(response.data.transactions)

        })
            .then(console.log)


        // @ts-ignore


    } catch (ex) {
        alert(ex)
    }

}


