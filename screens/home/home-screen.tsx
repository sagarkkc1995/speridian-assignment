import * as React from 'react'
import {useState, useEffect, useRef} from 'react'
import YearChartComponent from "./components/year-chart"
import {Button, StyleSheet, View} from 'react-native'
import LottieView from 'lottie-react-native'
import {styles} from './chart-styles'
import {getToken} from './service/chart-data-service'
import {getAccess} from './service/chart-data-service'
import {fireHook} from './service/chart-data-service'
import {retreiveTransactions} from './service/chart-data-service'

let jan = 0
let feb = 0
let march = 0
let april = 0
let may = 0
let june = 0
let july = 0
let august = 0
let sep = 0


export default function HomeScreen() {
    const [isDataLoaded, setDataLoaded] = useState(false)
    const [isYearPressed, setYearPressed] = useState(true)
    const [isSixMonthPressed, setSixMonthPressed] = useState(false)
    const [isNineMonthPressed, setNineMonthPressed] = useState(false)
    const [isThreeMonthPressed, setThreeMonthPressed] = useState(false)
    const loaderRef = useRef(null)


    const nineMonthData = {

        labels: ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sep"],
        datasets: [
            {
                data: [
                    jan,
                    feb,
                    march,
                    april,
                    may,
                    june,
                    july,
                    august,
                    sep
                ]
            }
        ]

    }
    const sixMonthData = {

        labels: ["January", "February", "March", "April", "May", "June"],
        datasets: [
            {
                data: [
                    jan,
                    feb,
                    march,
                    april,
                    may,
                    june


                ]
            }
        ]

    }

    const threeMonthData = {

        labels: ["July", "August", "September"],
        datasets: [
            {
                data: [
                    july,
                    august,
                    sep


                ]
            }
        ]

    }


    const yearData = {

        labels: ["Q1", "Q2", "Q3", "Q4"],
        datasets: [
            {
                data: [
                    jan + feb + march,
                    march + april + may,
                    may + june,
                    0


                ]
            }
        ]

    }


    const onYearPressed = () => {
        setYearPressed(true)
        setSixMonthPressed(false)
        setNineMonthPressed(false)
        setThreeMonthPressed(false)


    }

    const onSixMonthPressed = () => {

        setSixMonthPressed(true)
        setYearPressed(false)
        setNineMonthPressed(false)
        setThreeMonthPressed(false)

    }
    const onNineMonthPressed = () => {
        setNineMonthPressed(true)
        setSixMonthPressed(false)
        setYearPressed(false)
        setThreeMonthPressed(false)

    }

    const onThreeMonthPressed = () => {
        setNineMonthPressed(false)
        setSixMonthPressed(false)
        setYearPressed(false)
        setThreeMonthPressed(true)
    }

    const retreiveAccessToken = (data: any) => {
        getAccess(data, (response: any) => {
            deployWebHook(response)
            // @ts-ignore
        })

    }

    const deployWebHook = (data: any) => {
        fireHook(data, (response: any) => {
            getUserTransactions()
        })
    }

    const getUserTransactions = () => {
        retreiveTransactions((response: any) => {

            filterData(response)
        })
    }

    const filterData = (data: any) => {
        for (var i = 0; i < data.length; i++) {

            const date = new Date(data[i].date);  // 2009-11-10
            const month = date.toLocaleString('default', {month: 'long'});

            switch (month) {
                case "January" : {

                    jan += data[i].amount/100
                }
                case "February" : {

                    feb += data[i].amount/100
                }
                case "March" : {

                    march += data[i].amount/100
                }
                case "April" : {

                    april += data[i].amount/100
                }
                case "May" : {

                    may += data[i].amount/100
                }
                case "June" : {

                    june += data[i].amount/100
                }
                case "July" : {

                    july += data[i].amount/100
                }
                case "August" : {

                    august += data[i].amount/100
                }

                case "September" : {
                    sep += data[i].amount/100
                }

            }

        }
        setDataLoaded(true)
    }


    useEffect(() => {
        if (loaderRef.current) {
            // @ts-ignore
            loaderRef.current.play()
        }

        // getPublicToken()
        getToken((response: any) => {

            retreiveAccessToken(response)
            // @ts-ignore

        })

    }, [])


    // @ts-ignore
    return (
        <View style={styles.container}>

            {!isDataLoaded && <LottieView
                ref={loaderRef}
                style={styles.loaderStyle}
                source={require('./assets/loader.json')}
                // OR find more Lottie files @ https://lottiefiles.com/featured
                // Just click the one you like, place that file in the 'assets' folder to the left, and replace the above 'require' statement
            />}
            {isDataLoaded && isYearPressed && <YearChartComponent
                dataProp={yearData}
                isColoured={true}
            />}
            {isDataLoaded && isSixMonthPressed && <YearChartComponent
                dataProp={sixMonthData}
                isColoured={true}
            />}
            {isDataLoaded && isNineMonthPressed && <YearChartComponent
                dataProp={nineMonthData}
                isColoured={true}
            />}
            {isDataLoaded && isThreeMonthPressed && <YearChartComponent
                dataProp={threeMonthData}
                isColoured={true}
            />}
            {isDataLoaded && <View style={{flexDirection: 'row', backgroundColor: 'white'}}>

                <View style={{flex: 0.25,backgroundColor : isYearPressed ? 'yellow' : 'white'}}>
                    <Button title={"Year"} onPress={() => onYearPressed()}/>
                </View>
                <View style={{flex: 0.25,backgroundColor : isNineMonthPressed ? 'yellow' : 'white'}}>

                    <Button title={"9 M"} onPress={() => onNineMonthPressed()}/>
                </View>
                <View style={{flex: 0.25,backgroundColor : isSixMonthPressed ? 'yellow' : 'white'}}>

                    <Button title={"6 M"} onPress={() => onSixMonthPressed()}/>
                </View>
                <View style={{flex: 0.25,backgroundColor : isThreeMonthPressed ? 'yellow' : 'white'}}>

                    <Button title={"3 M"} onPress={() => onThreeMonthPressed()}/>
                </View>
            </View>}


        </View>
    );
}


